<?php

use Illuminate\Database\Seeder;

class BooksTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
            DB::table('books')->insert([[
            'title' => 'The Notebook',
            'author' => 'James',
            'created_at' => date('Y-m-d G:i:s'),
            'user_id'=>'1',
            'status'=>'0',
            ],
        
        
            [
            'title' => 'Kite Runner',
            'author' => 'John',
            'created_at' => date('Y-m-d G:i:s'),
            'user_id'=>'1',
            'status'=>'0',

            ],
        
        
            [
            'title' => 'Harry Potter',
            'author' => 'J.K.Rolling',
            'created_at' => date('Y-m-d G:i:s'),
            'user_id'=>'2',
            'status'=>'0',

            ],
        
        [
            'title' => 'The Hobit',
            'author' => 'J.R.Tolkin',
            'created_at' => date('Y-m-d G:i:s'),
            'user_id'=>'1',
            'status'=>'0',

        ],
        
        [

            'title' => 'Princess Diaries',
            'author' => 'Jane',
            'created_at' => date('Y-m-d G:i:s'),
            'user_id'=>'2',
            'status'=>'0',

        ]
        ]
            )
    ;}}