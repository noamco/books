<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert(
            [
                [
                'name' => 'Daniella Assaraf',
                'email' => 'Daniella@jce.ac.il',
                'password' =>Hash::make('123456789'),
                'role' => 'manager',
                'created_at' => date('Y-m-d G:i:s'),
                ],
                [
                'name' => 'Noam Cohen',
                'email' => 'Noam@jce.ac.il',    
                'password' =>Hash::make('123456789'),
                'role' => 'manager',
                'created_at' => date('Y-m-d G:i:s'),
                ],
                [
                'name' => 'Akiva Jacobs',
                'email' => 'Akiva.jce.ac.il',
                'password' =>Hash::make('123456789'),
                'role' => 'Employees',
                'created_at' => date('Y-m-d G:i:s'),
                ]
            ]);
            
        }

}
    