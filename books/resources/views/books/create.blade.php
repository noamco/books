@extends('layouts.app')
@section('content')
<h1>Create a new Task</h1>
<form method = 'post' action = "{{action('BookController@store')}}" >
{{csrf_field()}}

<div class = "form-group">
    <label for = "title"> Book Title</label>
    <input type= "text" class = "form-control" name = "title">
    <label for = "author"> Book Author</label>
    <input type= "text" class = "form-control" name = "author">
</div>
<input type = "submit" class = "form-control" name = "submit" value = "Save">
@if ($errors->any())
  <div class="alert alert-danger">
    <ul>
     @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
        @endforeach
        </ul>
        </div>
    @endif
</form>
@endsection