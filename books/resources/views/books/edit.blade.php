@extends('layouts.app')
@section('content')
<h1>Edit your task</h1>
<form method = 'post' action = "{{action('BookController@update',$book->id)}}" >
@csrf 
@method('PATCH')

<div class = "form-group">
    <label for = "title">Book Title</label>
    <input type= "text" class = "form-control" name = "title" value = "{{$book->title}}">
</div>
<div class = "form-group">
    <label for = "title">Book Author</label>
    <input type= "text" class = "form-control" name = "title" value = "{{$book->author}}">
</div>


<div class = "form-group">
<input type = "submit" class = "form-control" name = "submit" value = "Update">
</div>

</form>
<form method = 'post' action = "{{action('BookController@destroy',$book->id)}}" >
@csrf
@method('DELETE')

<div class = "form-group">
<input type = "submit" class = "form-control" name = "submit" value = "Delete">
</div>
@if ($errors->any())
  <div class="alert alert-danger">
    <ul>
     @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
        @endforeach
        </ul>
        </div>
    @endif
</form>

@endsection