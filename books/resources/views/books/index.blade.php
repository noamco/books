@extends('layouts.app')
@section('content')

<h1>This is your book list</h1>

  <table class="table">  
  <tr>
  
  <th>Title</th>
  <th>Author</th>
  <th>Status</th>
  </tr>

    @foreach($books as $book)
        <tr>
        <td> <a href = "{{route('books.edit',$book->id)}}">  {{$book->title}} </a> </td>
        <td> {{$book->author}}</td>

        <td>@if ($book->status)
           <input type = 'checkbox' id ="{{$book->id}}"  disabled='disable' checked>   
        @else
           <input type = 'checkbox' id ="{{$book->id}}">
       @endif
        </td>


        </tr>
            @endforeach

         @can('manager')
        <a href="{{route('books.create')}}" class=" btn btn-secondary">Add another books to your list</a>
        @endcan
</table>
  <script>
       $(document).ready(function(){
           $(":checkbox").click(function(event){
            console.log(event.target.id)


            $(this).attr('disabled', true);
            alert("Well done on finishing another book!");

               $.ajax({
                    url:"{{url('books')}}" + '/' + event.target.id,
                   dataType: 'json',
                   type:'put',
                   contentType: 'application/json',
                   data:  JSON.stringify({'status':event.target.checked, _token:'{{csrf_token()}}'}),
                   processData: false,
                   success: function( data){
                   },
                   error: function(errorThrown ){
                   }
               });               
           });
       });
   </script>  

@endsection
